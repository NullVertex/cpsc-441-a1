# CPSC 441 - Assignment 1 #

This program utilizes conditional get and a cache to download and store files from provided URL's.

## Usage ##
**Linux and BSD:**
Use the `make` command to compile all java files and generate documentation

**Windows:**
Use `javac *.java` to compile the java bytecode and `javadoc -d ./docs/ *.java` to generate documentation