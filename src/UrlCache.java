/**
 * UrlCache Class
 *
 * @author 	Majid Ghaderi
 * @version	1.0, Sep 22, 2015
 *
 */

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class UrlCache {

	private ArrayList<WebObject> objects;
	private File cache_file;
	private SimpleDateFormat sdf;

    /**
     * Default constructor to initialize data structures used for caching/etc
	 * If the cache already exists then load it. If any errors then throw exception.
	 *
     * @throws UrlCacheException if encounters any errors/exceptions
     */
	public UrlCache() throws UrlCacheException
	{
		objects = new ArrayList<WebObject>();
		cache_file = new File(".cache");

		// Used to parse date format from file
		sdf = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss");
		sdf.setLenient(false);

		if(cache_file.exists() && cache_file.canRead())
		{
			try
			{
				BufferedReader reader = new BufferedReader(new FileReader(cache_file));

				// Read each line of the file
				String line;
				while((line = reader.readLine()) != null)
				{
					// each line has the following format:
					// [hostname],[port],[resource],[last_modified]
					String[] parsed = line.split(",");
					try
					{
						// Load object from the file into local memory
						objects.add(new WebObject(parsed[0], Integer.parseInt(parsed[1]), parsed[2], sdf.parse(parsed[3])));
					}
					catch(ParseException e)
					{
						// Print an error if parsing the line fails.
						System.out.println("Error parsing line: " + line);
					}
				}

				// Close the file
				reader.close();
			}
			catch(IOException e)
			{
				// Throw an exception if reading the file fails.
				throw new UrlCacheException("Error Reading from cache file.");
			}
		}
	}

    /**
     * Downloads the object specified by the parameter url if the local copy is out of date.
	 *
     * @param url	URL of the object to be downloaded. It is a fully qualified URL.
     * @throws UrlCacheException if encounters any errors/exceptions
     */
	public void getObject(String url) throws UrlCacheException
	{
		WebObject current = new WebObject(url);
		for(int i=0; i < objects.size(); i++)
		{
			if(current.equals(objects.get(i)))
			{
				// Conditionally GET the object (ie. update if object is out of date only)
				objects.get(i).CGET();

				return;
			}
		}

		// If WebObject not already in cache, add it, and write it to the cache file
		WebObject entry = new WebObject(url);
		// Download the contents of the web object
		entry.CGET();
		// Add WebObject to current list of WebObjects in memory
		objects.add(entry);
		try
		{
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(cache_file)));

			// Write all WebObjects cached in memory to a local cache file
			for(int i = 0; i < objects.size(); i++)
			{
				writer.println(objects.get(i).toString());
			}

			// Close cache file
			writer.close();
		}
		catch(IOException e)
		{
			throw new UrlCacheException("Error writing to cache file.");
		}

		return;
	}

    /**
     * Returns the Last-Modified time associated with the object specified by the parameter url.
	 *
     * @param url 	URL of the object
	 * @return the Last-Modified time in millisecond as in Date.getTime()
     * @throws UrlCacheException if the specified url is not in the cache, or there are other errors/exceptions
     */
	public long getLastModified(String url) throws UrlCacheException
	{
		WebObject current = new WebObject(url);
		for(int i=0; i < objects.size(); i++)
		{
			if(current.equals(objects.get(i)))
			{
				return objects.get(i).getLastUpdate().getTime();
			}
		}
		throw new UrlCacheException("Item " + url + " was not found in the cache.");
	}
}
