/**
 * WebObject Class
 *
 * @author Cole Towstego
 * @version 1.0, Oct 08, 2015
 */

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WebObject
{
	private String hostname;
	private int port;
	private String path;
	private Date last_update;

	/**
	 * Create a new WebObject from a URL string. Uses creation time as "last updated" time.
	 *
	 * @param name A URL String used to create a new WebObject
	 */
	public WebObject(String name)
	{
		String temp = name;
		if(temp.contains("/"))
		{
			this.path = name.split("/", 2)[1];
			temp = temp.split("/", 2)[0];
		}

		if(temp.contains(":"))
		{
			// Specific port was included in URL, parse it out and use it
			this.port = Integer.parseInt(temp.split(":")[1]);
			this.hostname = temp.split(":")[0];
		}
		else
		{
			// No port specified, use defaults
			this.port = 80;
			this.hostname = temp;
		}

		// Set to oldest possible date, signifying that the file has never been updated.
		Date oldest = new Date();
		oldest.setTime(0);
		this.last_update = oldest;
	}

	/**
	 * Create a WebObject from existing parameters. Useful for restoring these objects from a file
	 *
	 * @param hostname Hostname (domain) where the resource is located
	 * @param port Port used to connect to the resource
	 * @param path Location of the resource on the host machine
	 * @param last_update the most recent time this object was updated
	 */
	public WebObject(String hostname, int port, String path, Date last_update)
	{
		// Default attributes, if not existant in original string
		this.hostname = hostname;
		this.port = port;
		this.path = path;
		this.last_update = last_update;
	}

	/**
	 * Returns the address of the host where the resource is located
	 *
	 * @return the hostname for the resource
	 */
	public String getHostname()
	{
		return this.hostname;
	}

	/**
	 * Returns the port to be used when downloading the WebObject
	 *
	 * @return port to be utilized when connecting to this web resource
	 */
	public int getPort()
	{
		return this.port;
	}

	/**
	 * Retuns the path of the specified resouce of the WebObject, located on the host machine
	 *
	 * @return path to the resource on host
	 */
	public String getPath()
	{
		return this.path;
	}

	/**
	 * Returns the last date the object was previously updated
	 *
	 * @return the date the object was last updated
	 */
	public Date getLastUpdate()
	{
		return this.last_update;
	}

	/**
	 * Compares WebObjects to determine if they are equivalent. WebObjects are considered equivalent if they share the same hostname, port, and resource path.
	 *
	 * @param w A WebObject to compare to.
	 * @return True if the WebObjects are considered to be equivalent
	 */
	public Boolean equals(WebObject w)
	{
		// Consider WebObjects equivalent if they share the same hostname, port, and resource path
		if(w.getHostname().equals(this.hostname) && w.getPort() == this.port && w.getPath().equals(this.path))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Converts WebObject to String, so that it can be written to a local cache file
	 *
	 * @return A String representation of the WebObject.
	 */
	public String toString()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss");
		return hostname + "," + port + "," + path + "," + sdf.format(last_update);
	}

	/**
	 * Uses conditional GET to download the file to the local cache. If the file is up to date with the one on the server, nothing will be done.
	 */
	public void CGET()
	{
		try
		{
			// Open a socket for connection
			Socket socket = new Socket(hostname, port);

			// Used to format date for requests and responses
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

			String request = "GET /" + path + " HTTP/1.1\r\nHost: " + hostname + "\r\nIf-Modified-Since: " + sdf.format(last_update) + "\r\n\r\n";
			// Submit "GET" request
			socket.getOutputStream().write(request.getBytes());
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// Read the HTTP response. If 200, download the file
			String line = reader.readLine();
			if(line.contains("200 OK"))
			{
				// Filename will be: [hostname]/[path]
				File file = new File(hostname + "/" + path);
				// Make filepath if it does not exist
				file.getParentFile().mkdirs();
				// Create a writer for the file
				FileOutputStream writer = new FileOutputStream(file);

				// # of bytes we need to read (Assume none, if not specified)
				int length = 0;
				last_update = null;

				// Read the HTTP response
				while((line = reader.readLine()) != null)
				{
					// Parse out the length of content needed to read
					if(line.contains("Content-Length:"))
					{
						length = Integer.parseInt(line.split(": ")[1]);
					}

					// Parse out the "last modified" date from the header
					if(line.contains("Last-Modified:"))
					{
						last_update = sdf.parse(line.split(": ")[1]);
					}

					// Read content after the header
					if(line.equals(""))
					{
						// Use the InputStream to directly read bytes
						InputStream iStream = socket.getInputStream();

						// For each byte in body section
						for(int i = 0; i < length; i++)
						{
							// Read the byte, or "segment"
							int segment = iStream.read();
							// If the segment is not the end of stream, write the segment
							if(segment != -1)
							{
								writer.write(segment);
							}
						}

						// Close the file writer and socket
						writer.close();
						socket.close();

						// If the http response didn't have a last modified time, use the current time
						if(last_update == null)
						{
							last_update = new Date();
						}

						// Done
						return;
					}
				}

			}
			else
			{
				// Print the http response code
				String httpResponse = line.substring(line.indexOf(" ") + 1);
				System.out.println("Response code: " + httpResponse);
			}
			socket.close();
		}
		catch(Exception e)
		{
			System.out.println("Error occured while updating.");
		}

		return;
	}
}
